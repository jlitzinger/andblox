package com.jlitzing.games.views;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.jlitzing.andblox.BloxGame;
import com.jlitzing.andblox.GameState;
import com.jlitzing.andblox.MainActivity;

public class GameSurface extends SurfaceView implements SurfaceHolder.Callback{

	private ExecutorService e;
	private BloxGame game;
	
	public GameSurface(Context context){
		super(context);
		Log.i(MainActivity.TAG,"single param constructor");
		SurfaceHolder holder = getHolder();
		holder.addCallback(this);
		game = new BloxGame(context);
	}
	
	public GameSurface(Context context,AttributeSet attrs){
		super(context,attrs);
		Log.i(MainActivity.TAG,"two param constructor");
		SurfaceHolder holder = getHolder();
		holder.addCallback(this);
		game = new BloxGame(context);
	}
	
	public GameSurface(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		Log.i(MainActivity.TAG,"three param constructor");
		SurfaceHolder holder = getHolder();
		holder.addCallback(this);
		game = new BloxGame(context);
	}

	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		
	}

	public static Future<?> f;
	
	public void surfaceCreated(SurfaceHolder holder) {
		Log.i(MainActivity.TAG,"Surface created");
		game.setHolder(holder);
		e = Executors.newFixedThreadPool(1);
		f = e.submit(game);
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		Log.i(MainActivity.TAG,"Surface destroyed");
		game.setHolder(null);
		e.shutdown();
	}

	public void toggleState(){
		if(game.getState() != GameState.Running){
			Log.i(MainActivity.TAG,"Starting game");
			game.start();
		}else{
			game.quit();
		}
	}
	
}
