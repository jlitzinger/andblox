package com.jlitzing.andblox.blocks;

public enum RotateDir {
	none,
	clockwise,
	counterClockwise;
}
