package com.jlitzing.andblox.blocks;

import android.graphics.Canvas;

public interface Block {

	public void translateRotate(float x,float y,RotateDir rotate);

	public void drawBlock(Canvas canvas);
	
}
