package com.jlitzing.andblox.blocks;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Point;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.util.Log;

import com.jlitzing.andblox.MainActivity;

/**
 * Implementation of a game 'block'.  The centroid of
 * the block is always relative to the GameBoard's coordinate
 * system.
 * 
 * The bounds are relative to the block's coordinate system and
 * the centroid of the block is at 0,0.  The values represent
 * a multiplier to apply to block width when setting the block
 * bounds.
 * E.g. 1,0 means xCentroid + (1 * block_width)
 * 
 * Every block can be rendered from two rectangles, which means
 * two shape drawables per block
 * 
 * @author jlitzing
 *
 */
public abstract class BlockImpl implements Block {

	protected float xCentroid;
	protected float yCentroid;
	protected RectShape r1;
	protected RectShape r2;
	protected ShapeDrawable d1;
	protected ShapeDrawable d2;
	protected List<Point> bounds = new ArrayList<Point>(4);
	
	
	public BlockImpl(){
		Log.i(MainActivity.TAG,"Just checkin");
		r1 = new RectShape();
		r2 = new RectShape();
		d1 = new ShapeDrawable(r1);
		d2 = new ShapeDrawable(r2);
		for(int i = 0;i < bounds.size();++i){
			bounds.add(new Point(0, 0));
		}
		
	}
	
//	public BlockImpl(BlockTypes blockType, float xCentroid, float yCentroid) {
//		
//		
//		bounds.add(new Point(-2, 0));
//		bounds.add(new Point(2, 0));
//		shape.resize(4* BlockFactory.BLOCK_WIDTH,BlockFactory.BLOCK_WIDTH);
//		Log.i(MainActivity.TAG,"bw=" + BlockFactory.BLOCK_WIDTH);
//	}
//
	public void translateRotate(float x, float y, RotateDir rotate) {
		xCentroid += x;
		yCentroid += y;
		if(rotate != RotateDir.none){
			Log.i(MainActivity.TAG,"Rotate");
		}
	}
//
//	public void drawBlock(Canvas canvas) {
//		int left = (int) (xCentroid + ((float) bounds.get(0).x * BLOCK_WIDTH)) - 1;
//		int right = (int) (xCentroid + ((float) bounds.get(1).x * BLOCK_WIDTH)) + 1;
//		
//		
//		int top =  (int) yCentroid;
//		int bottom = (int) (yCentroid + BLOCK_WIDTH) + 1;
//		drawable.setBounds(left, top, right, bottom);
//		drawable.draw(canvas);
//		
//	}
	
	/**
	 * Ok, so this works, by that I mean it will render the square blocks.  However, managing the
	 * shape drawables may be quite a bit more work than just managing a single drawable rendered a 
	 * few times.
	 */
	
}
