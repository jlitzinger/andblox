package com.jlitzing.andblox;

import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicReference;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.SurfaceHolder;

import com.jlitzing.andblox.blocks.Block;
import com.jlitzing.andblox.blocks.BlockFactory;

public class BloxGame implements Game, Runnable {

	private GameBoard board;
	private Block currentBlock;
	private Queue<BloxEvent> events;
	private BlockFactory blockFactory;
	private final AtomicReference<SurfaceHolder> holder = new AtomicReference<SurfaceHolder>(null);
	private AtomicReference<GameState> gameState = new AtomicReference<GameState>(GameState.Init);
	private long lastRun = 0;
	private final Resources resources;
	private final Paint startPaint = new Paint();
	
	
	public BloxGame(Context ctx){
		events = new ConcurrentLinkedQueue<BloxEvent>();
		this.resources = ctx.getResources();
		blockFactory = new BlockFactory();
		BlockFactory.BLOCK_WIDTH = resources.getDimension(R.dimen.block_width);
		startPaint.setColor(Color.WHITE);
		startPaint.setTextSize(resources.getDimension(R.dimen.text_size));
		board = new BloxBoard();
	}
	
	
	public void gameLoop(){
		Canvas canvas = null;

		//Since this could change from a different thread,
		//we want a constant view of it
		GameState state = gameState.get();
		if(state == GameState.Init){
			canvas = holder.get().lockCanvas();
			drawStart(canvas);
			holder.get().unlockCanvasAndPost(canvas);
			gameState.set(GameState.WaitingForStart);

		}else if(state == GameState.Running){

			canvas = holder.get().lockCanvas();
			resetCanvas(canvas);
			if(currentBlock == null){
				Log.i(MainActivity.TAG,"getBlock");
				currentBlock = blockFactory.getBlock();
			}
			while(events.peek() != null){
				BloxEvent e = events.poll();
				currentBlock.translateRotate(e.getTranslationXValue(),
						e.getTranslateYValue(),
						e.getRotateDir());

				if(board.checkCollisions(currentBlock)){
					currentBlock.translateRotate(e.getReverseXTranslation(),
							e.getReverseYTranslation(),
							e.getReverseRotateDir());

					if(e == BloxEvent.Drop){
						if(!board.addBlock(currentBlock)){
							gameState.set(GameState.GameOver);
						}
						currentBlock = null;
						break;
					}
				}
			}

			currentBlock.drawBlock(canvas);
			board.drawBoard(canvas);
			holder.get().unlockCanvasAndPost(canvas);

		}else if(state == GameState.GameOver){
			canvas = holder.get().lockCanvas();
			canvas.drawText(resources.getString(R.string.gameOver),
					10.0f * MainActivity.metrics.density,
					160.0f * MainActivity.metrics.density,
					startPaint);
			holder.get().unlockCanvasAndPost(canvas);
			gameState.set(GameState.WaitingForRestart);
		}

		try{
			Thread.sleep(1);
		}catch(InterruptedException e){
			e.printStackTrace();
		}
	}
	
	
	public void run() {
		long nanoNow = System.nanoTime();
		
		while(gameState.get() != GameState.WaitingForRestart){
			nanoNow = System.nanoTime();
			if((holder.get() != null)
				 && isReadyToRun(nanoNow)){
					lastRun = nanoNow;
					gameLoop();
			}else{
				try{
					Thread.sleep(2);
				}catch(InterruptedException e){
					e.printStackTrace();
				}
			}
		}
		
		
	}

	
	/**
	 * Add an event for the game to process.
	 * @return true if added, false otherwise
	 */
	public boolean addEvent(BloxEvent event) {
		return events.add(event);
	}

	public boolean start() {
		gameState.set(GameState.Running);
		return true;
	}

	public boolean quit() {
		gameState.set(GameState.GameOver);
		return true;
	}

	public GameState getState() {
		return gameState.get();
	}

	public void setHolder(SurfaceHolder holder) {
		this.holder.set(holder);
	}
	
	private void resetCanvas(Canvas c){
		c.drawColor(Color.BLACK);
	}

	/**
	 * Determine whether we should execute the game loop
	 * @param nanosNow - current system uptime
	 * @return true if we should execute, false otherwise.
	 */
	private boolean isReadyToRun(long nanosNow){
		if((nanosNow - lastRun) > 33000000L){
			return true;
		}else{
			return false;
		}
	}
	
	/**
	 * Draw the start message onto the surface view
	 * @param canvas
	 */
	private void drawStart(Canvas canvas){
		resetCanvas(canvas);
		canvas.drawText(resources.getString(R.string.start_text),
				resources.getDimension(R.dimen.start_xpos),
				resources.getDimension(R.dimen.start_ypos),
				startPaint);
	}



	
}
