package com.jlitzing.andblox;

import android.graphics.Canvas;

import com.jlitzing.andblox.blocks.Block;

public class BloxBoard implements GameBoard {

	public boolean checkCollisions(Block block) {
		return false;
	}

	public boolean isBoardFull() {
		return false;
	}

	public boolean addBlock(Block block) {
		return false;
	}

	public void drawBoard(Canvas canvas) {

	}

}
