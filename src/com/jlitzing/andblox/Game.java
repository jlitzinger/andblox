package com.jlitzing.andblox;

import android.view.SurfaceHolder;

public interface Game {

	public boolean addEvent(BloxEvent event);

	public boolean start();
	
	public boolean quit();
	
	public GameState getState();
	
	public void setHolder(SurfaceHolder holder);
	
}
