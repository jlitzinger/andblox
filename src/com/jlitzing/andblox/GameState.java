package com.jlitzing.andblox;

public enum GameState {
	Init,
	WaitingForStart,
	Running,
	GameOver,
	WaitingForRestart,

}
