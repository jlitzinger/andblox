package com.jlitzing.andblox;

import com.jlitzing.andblox.blocks.RotateDir;

public enum BloxEvent {
	MoveLeft("andblox.MOVE_LEFT",2.0f,0.0f,RotateDir.none),
	MoveRight("andblox.MOVE_RIGHT",-2.0f,0.0f,RotateDir.none),
	Rotate("andblox.ROTATE",0.0f,0.0f,RotateDir.clockwise),
	Drop("andblox.DROP",2.0f,0.0f,RotateDir.none),
	Quit("andblox.QUIT",0.0f,0.0f,RotateDir.none);
	
	private final String intent;
	private float translationXValue;
	private float reverseXTranslation;
	private float translateYValue;
	private float reverseYTranslation;
	private RotateDir rotateDir;
	private RotateDir reverseRotateDir;
	
	private BloxEvent(String intent,float xValue,float yValue,RotateDir rotate){
		this.intent = intent;
		this.translationXValue = xValue;
		this.translateYValue = yValue;
		this.reverseYTranslation = -1.0f * yValue;
		this.rotateDir = rotate;
		this.reverseXTranslation = -1.0f * translationXValue;
		if(rotate == RotateDir.clockwise){
			this.reverseRotateDir = RotateDir.counterClockwise;
		}
	}

	public float getTranslationXValue() {
		return translationXValue;
	}

	public float getTranslateYValue() {
		return translateYValue;
	}

	public float getReverseYTranslation() {
		return reverseYTranslation;
	}

	public float getReverseXTranslation() {
		return reverseXTranslation;
	}

	public RotateDir getReverseRotateDir() {
		return reverseRotateDir;
	}

	public RotateDir getRotateDir() {
		return rotateDir;
	}

	public String getIntent() {
		return intent;
	}
	
}
