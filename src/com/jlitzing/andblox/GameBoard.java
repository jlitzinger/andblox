package com.jlitzing.andblox;

import com.jlitzing.andblox.blocks.Block;

import android.graphics.Canvas;

public interface GameBoard {

	/**
	 * Check for collisions between a block and the board
	 * @param block - the block to check
	 * @return true if collisions are detected, false otherwise
	 */
	public boolean checkCollisions(Block block);
	
	public boolean isBoardFull();
	
	/**
	 * The only reason a block should fail to be added is if the board is
	 * full and we should be at Game Over.
	 * @param block block to add
	 * @return true if block added, false if board is full and game should end
	 */
	public boolean addBlock(Block block);
	
	public void drawBoard(Canvas canvas);
	
}
