package com.jlitzing.andblox;

import java.util.concurrent.ExecutionException;

import com.jlitzing.games.views.GameSurface;

import android.app.Activity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;

/**
 * Main activity for the blox game.  A few important notes:
 * 
 * - All coordinate values listed as constants in code are expressed as dp.
 * 
 * @author jlitzing
 *
 */
public class MainActivity extends Activity
{
	public static final String TAG = "AndBlox";
	public static final DisplayMetrics metrics = new DisplayMetrics();
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        Log.i(TAG,String.format("Metrics: h=%d,w=%d,density=%.2f",
        		(int) (metrics.heightPixels / metrics.density),
        		(int) (metrics.widthPixels/metrics.density),
        		metrics.density));
    }
    
	@Override
	protected void onPause() {
		super.onPause();
	}
    
	public void startStop(View v){
		Log.i(TAG,"Start stop");
		GameSurface s = (GameSurface) findViewById(R.id.surfaceView1);
		s.toggleState();
	}
    
	
	public void moveLeft(View v){
		Log.i(TAG,"checking");
		if(GameSurface.f.isDone()){
			Log.i(TAG,"Done");
			try {
				GameSurface.f.get();
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
		}
	}
}
